MKSHELL=mkx

test:V:	out/noimage-test.pdf	out/font-test.pdf	out/pandoc-test.pdf	out/template.pdf

out/noimage-test.pdf:V:	test/font-noimage.md
	cmd/prepare
	../cmd/build-doc
	   -o ../${target}
	   ../${prereq}

out/font-test.pdf:V:	test/font.md
	cmd/prepare
	../cmd/build-doc
	   -o ../${target}
	   ../${prereq}

out/pandoc-test.pdf:V:	test/document.md
	cmd/prepare
	../cmd/build-doc
	   -o ../${target}
	   ../${prereq}

out/template.pdf:V:	test/template.tex
	cmd/prepare
	xelatex ../${prereq}

clean:V:
	git clean -fd
