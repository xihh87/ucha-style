---
documentclass: scrartcl
grupo: Grupo del Instituto Nacional de Medicina Genómica
title: Ejemplo de Trabajo
author:
- Joshua Ismael Haase Hernández
- Some Other Author
profesor: Nombre del Profesor
asignatura: Nombre de la Asignatura
modulo: "Intermedio | Avanzado"
mainfont: DejaVu Sans
#fontfamily: "Arial"
fontsize: 12
links-as-notes: true
biblio-style: "apa"
date: 2019-10-05
toc: "true"
toc-title: "Contenido"
logo: ../test/ucha.png
---

Una sección
============

> Una cita textual que saqué de un libro muy importante
>
> [@gonzalez_nociones_1980]

Un subtítulo para la sección
------------------------------

Aunque nosotros como personas no establecimos estos acuerdos,
nos beneficiamos de lo que muchos otros antes que nosotros consideraron bueno.

Un párrafo seguido por una lista:

  - Las leyes
  - La jurisprudencia
  - Las costumbres
  - La historia

Otra sección
============

Los derechos son acuerdos sociales
y aunque no son inherentes al ser humano
como sociedad hemos acordado que por el hecho de ser humano
cada persona debería tener garantizadas ciertas condiciones mínimas.

```
Código que a veces uso, pero en estos trabajos casi no
```

## El estado debe garantizar la salud porque es difícil atender la salud de otra manera

Para esto hay tres sistemas de gobierno que aseguran a la población
dependiendo de su empleo (OCDE 2016).

![Una imagen con texto y bibliografía [@coneval_estudio_2018]](../test/ucha.png)

Sin embargo se han identificado varios problemas para eficientar el uso de los recursos dedicados en México a la salud [@oecd_oecd_2016]\:
